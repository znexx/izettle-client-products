# Swagger\Client\ImportApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getLatestImportStatus**](ImportApi.md#getLatestImportStatus) | **GET** /organizations/{organizationUuid}/import/status | Get Status for latest import
[**getStatusByUuid**](ImportApi.md#getStatusByUuid) | **GET** /organizations/{organizationUuid}/import/status/{importUuid} | Get Status for import by uuid
[**importLibraryV2**](ImportApi.md#importLibraryV2) | **POST** /organizations/{organizationUuid}/import/v2 | Bulk import items (Products and Discounts)


# **getLatestImportStatus**
> \Swagger\Client\Model\ImportResponse getLatestImportStatus($organization_uuid)

Get Status for latest import



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\ImportApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID

try {
    $result = $apiInstance->getLatestImportStatus($organization_uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ImportApi->getLatestImportStatus: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |

### Return type

[**\Swagger\Client\Model\ImportResponse**](../Model/ImportResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getStatusByUuid**
> \Swagger\Client\Model\ImportResponse getStatusByUuid($organization_uuid, $import_uuid)

Get Status for import by uuid



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\ImportApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$import_uuid = "import_uuid_example"; // string | Import id as an UUID

try {
    $result = $apiInstance->getStatusByUuid($organization_uuid, $import_uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ImportApi->getStatusByUuid: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **import_uuid** | [**string**](../Model/.md)| Import id as an UUID |

### Return type

[**\Swagger\Client\Model\ImportResponse**](../Model/ImportResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **importLibraryV2**
> \Swagger\Client\Model\ImportResponse importLibraryV2($organization_uuid, $body)

Bulk import items (Products and Discounts)

Import products and Discounts

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\ImportApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$organization_uuid = "organization_uuid_example"; // string | 
$body = new \Swagger\Client\Model\JsonNode(); // \Swagger\Client\Model\JsonNode | 

try {
    $result = $apiInstance->importLibraryV2($organization_uuid, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ImportApi->importLibraryV2: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)|  |
 **body** | [**\Swagger\Client\Model\JsonNode**](../Model/JsonNode.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\ImportResponse**](../Model/ImportResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

