# Swagger\Client\DiscountsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createDiscount**](DiscountsApi.md#createDiscount) | **POST** /organizations/{organizationUuid}/discounts | Creates a single discount entity
[**deleteDiscount**](DiscountsApi.md#deleteDiscount) | **DELETE** /organizations/{organizationUuid}/discounts/{discountUuid} | Deletes a single discount entity
[**getAllDiscounts**](DiscountsApi.md#getAllDiscounts) | **GET** /organizations/{organizationUuid}/discounts | Retrieves all discount
[**getDiscount**](DiscountsApi.md#getDiscount) | **GET** /organizations/{organizationUuid}/discounts/{discountUuid} | Retrieves a single discount entity
[**updateDiscount**](DiscountsApi.md#updateDiscount) | **PUT** /organizations/{organizationUuid}/discounts/{discountUuid} | Updates a single discount entity


# **createDiscount**
> createDiscount($organization_uuid, $body)

Creates a single discount entity

Crates a single discount entity. The location of the newly created discount will be available in the successful response as a Location header

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\DiscountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$body = new \Swagger\Client\Model\DiscountRequest(); // \Swagger\Client\Model\DiscountRequest | The new discount to create

try {
    $apiInstance->createDiscount($organization_uuid, $body);
} catch (Exception $e) {
    echo 'Exception when calling DiscountsApi->createDiscount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **body** | [**\Swagger\Client\Model\DiscountRequest**](../Model/DiscountRequest.md)| The new discount to create |

### Return type

void (empty response body)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteDiscount**
> deleteDiscount($organization_uuid, $discount_uuid)

Deletes a single discount entity

Deletes a discount entity

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\DiscountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$discount_uuid = "discount_uuid_example"; // string | Discount Identifier as an UUID

try {
    $apiInstance->deleteDiscount($organization_uuid, $discount_uuid);
} catch (Exception $e) {
    echo 'Exception when calling DiscountsApi->deleteDiscount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **discount_uuid** | [**string**](../Model/.md)| Discount Identifier as an UUID |

### Return type

void (empty response body)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAllDiscounts**
> \Swagger\Client\Model\Discount[] getAllDiscounts($organization_uuid)

Retrieves all discount

Get all discount entities that the authorized user has access to.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\DiscountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$organization_uuid = "organization_uuid_example"; // string | 

try {
    $result = $apiInstance->getAllDiscounts($organization_uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountsApi->getAllDiscounts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)|  |

### Return type

[**\Swagger\Client\Model\Discount[]**](../Model/Discount.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getDiscount**
> \Swagger\Client\Model\Discount getDiscount($organization_uuid, $discount_uuid)

Retrieves a single discount entity

Get the full discount with the provided UUID. The method supports conditional GET through providing a If-None-Match header. If the conditional prerequisite is fullfilled, the full discount is returned: otherwise a 304 not modified will be returned with an empty body.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\DiscountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$discount_uuid = "discount_uuid_example"; // string | Discount Identifier as an UUID

try {
    $result = $apiInstance->getDiscount($organization_uuid, $discount_uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DiscountsApi->getDiscount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **discount_uuid** | [**string**](../Model/.md)| Discount Identifier as an UUID |

### Return type

[**\Swagger\Client\Model\Discount**](../Model/Discount.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateDiscount**
> updateDiscount($organization_uuid, $discount_uuid, $body)

Updates a single discount entity

Updates a discount entity using JSON merge patch (https://tools.ietf.org/html/rfc7386). This means that only included fields will be changed: null values removes the field on the target entity, and other values updates the field.  Conditional updates are supported through the If-Match header. If the conditional prerequisite is fullfilled, the discount is updated: otherwise a 412 precondition failed will be returned with an empty body.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\DiscountsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$discount_uuid = "discount_uuid_example"; // string | Discount Identifier as an UUID
$body = new \Swagger\Client\Model\DiscountRequest(); // \Swagger\Client\Model\DiscountRequest | The new version of the discount to update, partials of the object can be provided as per JSON merge patch

try {
    $apiInstance->updateDiscount($organization_uuid, $discount_uuid, $body);
} catch (Exception $e) {
    echo 'Exception when calling DiscountsApi->updateDiscount: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **discount_uuid** | [**string**](../Model/.md)| Discount Identifier as an UUID |
 **body** | [**\Swagger\Client\Model\DiscountRequest**](../Model/DiscountRequest.md)| The new version of the discount to update, partials of the object can be provided as per JSON merge patch |

### Return type

void (empty response body)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

