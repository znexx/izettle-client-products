# Swagger\Client\ProductsonlineApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**activateOnline**](ProductsonlineApi.md#activateOnline) | **POST** /organizations/{organizationUuid}/products/online/migrate | Set Online Active on all products
[**createSlugFromProductName**](ProductsonlineApi.md#createSlugFromProductName) | **POST** /organizations/{organizationUuid}/products/online/slug | Creates a unique Slug (identifier) for a product. Slug is used for creating  URL to the product


# **activateOnline**
> activateOnline($organization_uuid)

Set Online Active on all products



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\ProductsonlineApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID

try {
    $apiInstance->activateOnline($organization_uuid);
} catch (Exception $e) {
    echo 'Exception when calling ProductsonlineApi->activateOnline: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |

### Return type

void (empty response body)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createSlugFromProductName**
> \Swagger\Client\Model\SlugResponse createSlugFromProductName($organization_uuid, $body)

Creates a unique Slug (identifier) for a product. Slug is used for creating  URL to the product



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\ProductsonlineApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$body = new \Swagger\Client\Model\CreateSlugRequest(); // \Swagger\Client\Model\CreateSlugRequest | Product name

try {
    $result = $apiInstance->createSlugFromProductName($organization_uuid, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsonlineApi->createSlugFromProductName: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **body** | [**\Swagger\Client\Model\CreateSlugRequest**](../Model/CreateSlugRequest.md)| Product name | [optional]

### Return type

[**\Swagger\Client\Model\SlugResponse**](../Model/SlugResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

