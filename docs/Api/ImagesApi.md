# Swagger\Client\ImagesApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAllImageUrls**](ImagesApi.md#getAllImageUrls) | **GET** /organizations/{organizationUuid}/images | Retrieves all library items images used by the organization. Sorted by date updated


# **getAllImageUrls**
> \Swagger\Client\Model\LibraryImagesResponse getAllImageUrls()

Retrieves all library items images used by the organization. Sorted by date updated



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\ImagesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->getAllImageUrls();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ImagesApi->getAllImageUrls: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\LibraryImagesResponse**](../Model/LibraryImagesResponse.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

