# Swagger\Client\ProductsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createProduct1**](ProductsApi.md#createProduct1) | **POST** /organizations/{organizationUuid}/products | Creates a single product entity
[**deleteProduct1**](ProductsApi.md#deleteProduct1) | **DELETE** /organizations/{organizationUuid}/products/{productUuid} | Deletes a single product entity
[**deleteProducts**](ProductsApi.md#deleteProducts) | **DELETE** /organizations/{organizationUuid}/products | Bulk delete product entities
[**getAllProducts**](ProductsApi.md#getAllProducts) | **GET** /organizations/{organizationUuid}/products/v2 | Retrieves all products
[**getAllProductsInPos**](ProductsApi.md#getAllProductsInPos) | **GET** /organizations/{organizationUuid}/products | 
[**getProduct1**](ProductsApi.md#getProduct1) | **GET** /organizations/{organizationUuid}/products/{productUuid} | Retrieves a single product entity
[**updateFullProduct**](ProductsApi.md#updateFullProduct) | **PUT** /organizations/{organizationUuid}/products/v2/{productUuid} | Updates a single product entity


# **createProduct1**
> createProduct1($organization_uuid, $body, $return_entity)

Creates a single product entity

Creates a single product entity. The location of the newly created product will be available in the successful response as a Location header. Returns the newly created entity if queryParam 'returnEntity' is set to true

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$body = new \Swagger\Client\Model\ProductCreateRequest(); // \Swagger\Client\Model\ProductCreateRequest | The product to create
$return_entity = false; // bool | 

try {
    $apiInstance->createProduct1($organization_uuid, $body, $return_entity);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->createProduct1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **body** | [**\Swagger\Client\Model\ProductCreateRequest**](../Model/ProductCreateRequest.md)| The product to create |
 **return_entity** | **bool**|  | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteProduct1**
> deleteProduct1($organization_uuid, $product_uuid)

Deletes a single product entity

Deletes a product entity

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$product_uuid = "product_uuid_example"; // string | Product Identifier as an UUID

try {
    $apiInstance->deleteProduct1($organization_uuid, $product_uuid);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->deleteProduct1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **product_uuid** | [**string**](../Model/.md)| Product Identifier as an UUID |

### Return type

void (empty response body)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteProducts**
> deleteProducts($organization_uuid, $uuid)

Bulk delete product entities

Build delete product entities.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$uuid = array("uuid_example"); // string[] | List of products to delete

try {
    $apiInstance->deleteProducts($organization_uuid, $uuid);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->deleteProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **uuid** | [**string[]**](../Model/string.md)| List of products to delete |

### Return type

void (empty response body)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAllProducts**
> \Swagger\Client\Model\Product[] getAllProducts($organization_uuid, $sort)

Retrieves all products

Get all product entities that the authorized user has access to. The response might be fairly large, so clients with limited capacity might want to use the library endpoint instead

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$sort = true; // bool | Sorts result based on created timestamp

try {
    $result = $apiInstance->getAllProducts($organization_uuid, $sort);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->getAllProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **sort** | **bool**| Sorts result based on created timestamp | [optional]

### Return type

[**\Swagger\Client\Model\Product[]**](../Model/Product.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAllProductsInPos**
> \Swagger\Client\Model\Product[] getAllProductsInPos($organization_uuid)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | 

try {
    $result = $apiInstance->getAllProductsInPos($organization_uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->getAllProductsInPos: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)|  |

### Return type

[**\Swagger\Client\Model\Product[]**](../Model/Product.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getProduct1**
> \Swagger\Client\Model\Product getProduct1($organization_uuid, $product_uuid)

Retrieves a single product entity

Get the full product with the provided UUID. The method supports conditional GET through providing a If-None-Match header. If the conditional prerequisite is fullfilled, the full product is returned: otherwise a 304 not modified will be returned with an empty body.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$product_uuid = "product_uuid_example"; // string | Product Identifier as an UUID

try {
    $result = $apiInstance->getProduct1($organization_uuid, $product_uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->getProduct1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **product_uuid** | [**string**](../Model/.md)| Product Identifier as an UUID |

### Return type

[**\Swagger\Client\Model\Product**](../Model/Product.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateFullProduct**
> \Swagger\Client\Model\Product updateFullProduct($organization_uuid, $product_uuid, $body)

Updates a single product entity

Updates a product entity using JSON merge patch (https://tools.ietf.org/html/rfc7386). This means that only included fields will be changed: null values removes the field on the target entity, and other values updates the field.  Conditional updates are supported through the If-Match header. If the conditional prerequisite is fullfilled, the product is updated: otherwise a 412 precondition failed will be returned with an empty body.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$product_uuid = "product_uuid_example"; // string | Product Identifier as an UUID
$body = new \Swagger\Client\Model\FullProductUpdateRequest(); // \Swagger\Client\Model\FullProductUpdateRequest | The new version of the product to update, partials of the object can be provided as per JSON merge patch

try {
    $result = $apiInstance->updateFullProduct($organization_uuid, $product_uuid, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->updateFullProduct: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **product_uuid** | [**string**](../Model/.md)| Product Identifier as an UUID |
 **body** | [**\Swagger\Client\Model\FullProductUpdateRequest**](../Model/FullProductUpdateRequest.md)| The new version of the product to update, partials of the object can be provided as per JSON merge patch |

### Return type

[**\Swagger\Client\Model\Product**](../Model/Product.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

