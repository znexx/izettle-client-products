# Swagger\Client\SyncApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createProduct**](SyncApi.md#createProduct) | **POST** /organizations/{organizationUuid}/sync/products | Creates a single product
[**deleteAllWithSource**](SyncApi.md#deleteAllWithSource) | **DELETE** /organizations/{organizationUuid}/sync/products/source/{source} | 
[**deleteProduct**](SyncApi.md#deleteProduct) | **DELETE** /organizations/{organizationUuid}/sync/products/{productUuid} | Deletes a single product
[**getProduct**](SyncApi.md#getProduct) | **GET** /organizations/{organizationUuid}/sync/products/{productUuid} | Retrieves a single product entity
[**setIsInternallyHandled**](SyncApi.md#setIsInternallyHandled) | **GET** /organizations/{organizationUuid}/sync/products/source/{source}/set-internal | 
[**updateProduct**](SyncApi.md#updateProduct) | **PATCH** /organizations/{organizationUuid}/sync/products/{productUuid} | Updates a single product


# **createProduct**
> createProduct($body, $organization_uuid, $store_provider, $return_entity)

Creates a single product

Creates a single product. Returns the created entity if the queryParam 'returnEntity' is set to true

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\SyncApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Swagger\Client\Model\ProductCreateRequest(); // \Swagger\Client\Model\ProductCreateRequest | The product to create
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$store_provider = "store_provider_example"; // string | 
$return_entity = false; // bool | 

try {
    $apiInstance->createProduct($body, $organization_uuid, $store_provider, $return_entity);
} catch (Exception $e) {
    echo 'Exception when calling SyncApi->createProduct: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\ProductCreateRequest**](../Model/ProductCreateRequest.md)| The product to create |
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **store_provider** | **string**|  |
 **return_entity** | **bool**|  | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteAllWithSource**
> deleteAllWithSource($organization_uuid, $source)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SyncApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | 
$source = "source_example"; // string | 

try {
    $apiInstance->deleteAllWithSource($organization_uuid, $source);
} catch (Exception $e) {
    echo 'Exception when calling SyncApi->deleteAllWithSource: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)|  |
 **source** | **string**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteProduct**
> deleteProduct($organization_uuid, $product_uuid, $store_provider)

Deletes a single product

Deletes a product

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\SyncApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$product_uuid = "product_uuid_example"; // string | productUuid
$store_provider = "store_provider_example"; // string | 

try {
    $apiInstance->deleteProduct($organization_uuid, $product_uuid, $store_provider);
} catch (Exception $e) {
    echo 'Exception when calling SyncApi->deleteProduct: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **product_uuid** | [**string**](../Model/.md)| productUuid |
 **store_provider** | **string**|  |

### Return type

void (empty response body)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getProduct**
> \Swagger\Client\Model\Product getProduct($organization_uuid, $product_uuid)

Retrieves a single product entity

Get the full product with the provided UUID. The method supports conditional GET through providing a If-None-Match header. If the conditional prerequisite is fullfilled, the full product is returned: otherwise a 304 not modified will be returned with an empty body.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\SyncApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$product_uuid = "product_uuid_example"; // string | Product Identifier as an UUID

try {
    $result = $apiInstance->getProduct($organization_uuid, $product_uuid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SyncApi->getProduct: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **product_uuid** | [**string**](../Model/.md)| Product Identifier as an UUID |

### Return type

[**\Swagger\Client\Model\Product**](../Model/Product.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **setIsInternallyHandled**
> setIsInternallyHandled($organization_uuid, $source)



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SyncApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$organization_uuid = "organization_uuid_example"; // string | 
$source = "source_example"; // string | 

try {
    $apiInstance->setIsInternallyHandled($organization_uuid, $source);
} catch (Exception $e) {
    echo 'Exception when calling SyncApi->setIsInternallyHandled: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)|  |
 **source** | **string**|  |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateProduct**
> updateProduct($organization_uuid, $product_uuid, $store_provider, $body, $return_entity)

Updates a single product

Updates a product using JSON merge patch (https://tools.ietf.org/html/rfc7386). This means that only included fields will be changed: null values removes the field on the target entity, and other values updates the field. Conditional updates are supported through the If-Match header. If the conditional prerequisite is fullfilled, the product is updated: otherwise a 412 precondition failed will be returned with an empty body.The metadata field will not be updated when syncing from Shopify as metadata.inPos is handled from iZettle to set the product visibility in the PoS.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\SyncApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$product_uuid = "product_uuid_example"; // string | productUuid
$store_provider = "store_provider_example"; // string | 
$body = new \Swagger\Client\Model\FullProductUpdateRequest(); // \Swagger\Client\Model\FullProductUpdateRequest | The new version of the product to update, partials of the object can be provided as per JSON merge patch
$return_entity = false; // bool | 

try {
    $apiInstance->updateProduct($organization_uuid, $product_uuid, $store_provider, $body, $return_entity);
} catch (Exception $e) {
    echo 'Exception when calling SyncApi->updateProduct: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **product_uuid** | [**string**](../Model/.md)| productUuid |
 **store_provider** | **string**|  |
 **body** | [**\Swagger\Client\Model\FullProductUpdateRequest**](../Model/FullProductUpdateRequest.md)| The new version of the product to update, partials of the object can be provided as per JSON merge patch |
 **return_entity** | **bool**|  | [optional] [default to false]

### Return type

void (empty response body)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

