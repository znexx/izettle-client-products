# Swagger\Client\LibraryApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getLibrarySnapshot**](LibraryApi.md#getLibrarySnapshot) | **GET** /organizations/{organizationUuid}/library | Retrieves the entire library


# **getLibrarySnapshot**
> \Swagger\Client\Model\Library getLibrarySnapshot($organization_uuid, $event_log_uuid, $limit, $offset, $all)

Retrieves the entire library

Will return the entire library for the authenticated user. If size of the library exceeds server preferences (normally 500) or the value of the optional limit parameter, the result will be paginated. Pagination is indicated by returning a Link header, indicating next URI to fetch.  The resulting header value will look something like: '<https://products.izettle.com/organizations/self/library?limit=X&offset=Y>; rel=\"next\"' where limit is number of items in response, and offset is current position in pagination. The rel-part in the header is the links relation to the data previously recieved. The idea is as long as this header is present there are still items remaining to be fetched. When either the header is not present or it's value doesn't contain any \"next\" value, all items have been sent to the client.  Note: The client should NOT try to extract query parameters from the IRI, but rather use it as-is for the next request. Also, clients should be perpared that one Link header might contain multiple other IRIs that are not \"next\" (there will never be more than one \"next\" though).  See more at:  IETF: https://tools.ietf.org/html/rfc5988  GitHub: https://developer.github.com/guides/traversing-with-pagination/ If eventLogUuid is provided, the response will only include events affecting the library since that event. Such responses are normally quite small and would be a prefered method for most fat clients after retrieving the initial full library

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure OAuth2 access token for authorization: oauth2
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');

$apiInstance = new Swagger\Client\Api\LibraryApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$organization_uuid = "organization_uuid_example"; // string | Organization Identifier as an UUID
$event_log_uuid = "event_log_uuid_example"; // string | The uuid of the earliest eventlog already known to the client
$limit = 500; // int | The max number of items returned per request
$offset = "offset_example"; // string | The offset within the current snapshot for the current page
$all = true; // bool | Return all products, otherwise return only the ones visible in the POS

try {
    $result = $apiInstance->getLibrarySnapshot($organization_uuid, $event_log_uuid, $limit, $offset, $all);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LibraryApi->getLibrarySnapshot: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **organization_uuid** | [**string**](../Model/.md)| Organization Identifier as an UUID |
 **event_log_uuid** | [**string**](../Model/.md)| The uuid of the earliest eventlog already known to the client | [optional]
 **limit** | **int**| The max number of items returned per request | [optional] [default to 500]
 **offset** | **string**| The offset within the current snapshot for the current page | [optional]
 **all** | **bool**| Return all products, otherwise return only the ones visible in the POS | [optional]

### Return type

[**\Swagger\Client\Model\Library**](../Model/Library.md)

### Authorization

[oauth2](../../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

