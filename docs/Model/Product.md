# Product

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **string** |  | 
**categories** | **string[]** |  | [optional] 
**name** | **string** |  | 
**description** | **string** |  | [optional] 
**image_lookup_keys** | **string[]** |  | [optional] 
**presentation** | [**\Swagger\Client\Model\Presentation**](Presentation.md) | Properties related to visual presentation | [optional] 
**variants** | [**\Swagger\Client\Model\Variant[]**](Variant.md) |  | 
**external_reference** | **string** |  | [optional] 
**etag** | **string** | Unique id of the change | 
**updated** | **int** | When producgt was updated. Example 2018-01-16T12:37:10.494+0000 | 
**updated_by** | **string** |  | [optional] 
**created** | **string** | When product was created. Example 2018-01-10T12:37:10.000+0000 | 
**unit_name** | **string** |  | [optional] 
**vat_percentage** | **string** | Vat, with max one decimal | [optional] 
**online** | [**\Swagger\Client\Model\Online**](Online.md) | Online specific properties | [optional] 
**variant_option_definitions** | [**\Swagger\Client\Model\VariantOptionDefinitions**](VariantOptionDefinitions.md) |  | [optional] 
**tax_code** | **string** |  | [optional] 
**category** | [**\Swagger\Client\Model\CategoryDTO**](CategoryDTO.md) |  | [optional] 
**metadata** | [**\Swagger\Client\Model\MetadataDTO**](MetadataDTO.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


