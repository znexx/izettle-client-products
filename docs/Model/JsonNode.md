# JsonNode

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**float** | **bool** |  | [optional] 
**array** | **bool** |  | [optional] 
**empty** | **bool** |  | [optional] 
**null** | **bool** |  | [optional] 
**floating_point_number** | **bool** |  | [optional] 
**value_node** | **bool** |  | [optional] 
**container_node** | **bool** |  | [optional] 
**missing_node** | **bool** |  | [optional] 
**object** | **bool** |  | [optional] 
**node_type** | **string** |  | [optional] 
**pojo** | **bool** |  | [optional] 
**number** | **bool** |  | [optional] 
**integral_number** | **bool** |  | [optional] 
**short** | **bool** |  | [optional] 
**int** | **bool** |  | [optional] 
**long** | **bool** |  | [optional] 
**double** | **bool** |  | [optional] 
**big_decimal** | **bool** |  | [optional] 
**big_integer** | **bool** |  | [optional] 
**textual** | **bool** |  | [optional] 
**boolean** | **bool** |  | [optional] 
**binary** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


