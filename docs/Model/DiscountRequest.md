# DiscountRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **string** |  | 
**name** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**amount** | [**\Swagger\Client\Model\Price**](Price.md) |  | [optional] 
**percentage** | **string** |  | [optional] 
**image_lookup_keys** | **string[]** |  | [optional] 
**external_reference** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


