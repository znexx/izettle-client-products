# FullProductUpdateRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **string** |  | 
**categories** | **string[]** |  | [optional] 
**name** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**image_lookup_keys** | **string** |  | [optional] 
**presentation** | [**\Swagger\Client\Model\Presentation**](Presentation.md) |  | [optional] 
**variants** | [**\Swagger\Client\Model\Variant[]**](Variant.md) |  | [optional] 
**external_reference** | **string** |  | [optional] 
**unit_name** | **string** |  | [optional] 
**vat_percentage** | **string** |  | [optional] 
**online** | [**\Swagger\Client\Model\Online**](Online.md) |  | [optional] 
**variant_option_definitions** | [**\Swagger\Client\Model\VariantOptionDefinitions**](VariantOptionDefinitions.md) |  | [optional] 
**tax_code** | **string** |  | [optional] 
**category** | [**\Swagger\Client\Model\CategoryDTO**](CategoryDTO.md) |  | [optional] 
**metadata** | [**\Swagger\Client\Model\MetadataDTO**](MetadataDTO.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


