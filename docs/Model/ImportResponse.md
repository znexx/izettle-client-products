# ImportResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **string** |  | [optional] 
**state** | **string** |  | [optional] 
**items** | **int** |  | [optional] 
**created** | **int** |  | [optional] 
**finished** | **int** |  | [optional] 
**error_message** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


