# Presentation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**image_url** | **string** |  | [optional] 
**background_color** | **string** |  | [optional] 
**text_color** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


