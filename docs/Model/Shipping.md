# Shipping

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shipping_pricing_model** | **string** |  | [optional] 
**weight_in_grams** | **int** | Weight in grams. | [optional] 
**weight** | [**\Swagger\Client\Model\Weight**](Weight.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


