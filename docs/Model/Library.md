# Library

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**until_event_log_uuid** | **string** |  | [optional] 
**from_event_log_uuid** | **string** |  | [optional] 
**products** | [**\Swagger\Client\Model\Product[]**](Product.md) |  | [optional] 
**discounts** | [**\Swagger\Client\Model\Discount[]**](Discount.md) |  | [optional] 
**deleted_products** | **string[]** |  | [optional] 
**deleted_discounts** | **string[]** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


