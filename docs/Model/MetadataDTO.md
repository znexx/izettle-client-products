# MetadataDTO

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**in_pos** | **bool** |  | 
**source** | [**\Swagger\Client\Model\SourceDTO**](SourceDTO.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


