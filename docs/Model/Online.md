# Online

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **string** |  | 
**title** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**shipping** | [**\Swagger\Client\Model\Shipping**](Shipping.md) |  | [optional] 
**presentation** | [**\Swagger\Client\Model\Presentation**](Presentation.md) |  | [optional] 
**seo** | [**\Swagger\Client\Model\SearchEngineOptimization**](SearchEngineOptimization.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


