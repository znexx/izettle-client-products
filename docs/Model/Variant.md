# Variant

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uuid** | **string** |  | 
**name** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**sku** | **string** |  | [optional] 
**barcode** | **string** |  | [optional] 
**price** | [**\Swagger\Client\Model\Price**](Price.md) |  | [optional] 
**cost_price** | [**\Swagger\Client\Model\Price**](Price.md) |  | [optional] 
**vat_percentage** | **float** |  | [optional] 
**options** | [**\Swagger\Client\Model\VariantOption[]**](VariantOption.md) |  | [optional] 
**presentation** | [**\Swagger\Client\Model\Presentation**](Presentation.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


