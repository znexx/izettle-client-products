<?php
/**
 * ModelInterface
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client\Model
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * API documentation for iZettle Product Library
 *
 * A product library is a representation of all the items that can be displayed, put in a shopping cart  and sold to a customer. Items may be either producs or discounts.  A product is a syntethic construct, wrapping one or more variants (which is the actual item being sold). Variants expresses different variations of properties such as for example price, size or color.  A discount will reduce the total amount charged in a shopping cart. It can be used per item line, or on the whole cart. It may reduce the affected amount by a percentage, or by a fixed amount.  Together, the above types of entities makes up a complete library. The library can be fetched as a whole through the library endpoint, where each consecutive change applied to the library is available. Once the full library is retrieved, only later events needs to be fetched to keep the client up to date with the server.  All path patterns \"/organizations/{organizationUuid}/\" can be replaced by \"/organizations/self/\" for convenience as all endpoints are for authorized users.
 *
 * OpenAPI spec version: v1.0
 * Contact: manage-team@izettle.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.12
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

/**
 * Interface abstracting model access.
 *
 * @package Swagger\Client\Model
 * @author  Swagger Codegen team
 */
interface ModelInterface
{
    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName();

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes();

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats();

    /**
     * Array of attributes where the key is the local name, and the value is the original name
     *
     * @return array
     */
    public static function attributeMap();

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters();

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters();

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array
     */
    public function listInvalidProperties();

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool
     */
    public function valid();
}
